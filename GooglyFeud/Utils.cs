﻿using System;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;

namespace GooglyFeud
{
	public static class Utils
	{
		public static List<string> FetchQueriesFromGoogle(string query)
		{
			//todo: validate + is what I want
			var queryNoSpaces = query.Replace(" ", "+");

			var httpClient = new HttpClient();
			HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, string.Format("http://google.com/complete/search?client=chrome&q={0}", queryNoSpaces));

			var response = httpClient.SendAsync(request).Result;

			string rawJsonContent = response.Content.ReadAsStringAsync().Result;

			//hacky way to clip out the content I want from their strange json object:
			rawJsonContent = rawJsonContent.Substring(rawJsonContent.IndexOf(','));
			var start = rawJsonContent.IndexOf('[');
			var length = rawJsonContent.IndexOf(']') - start + 1;
			var jsonContent = rawJsonContent.Substring(start, length);

			var autocompleteList = JsonConvert.DeserializeObject<List<string>>(jsonContent);
			return autocompleteList;
		}
	}
}

