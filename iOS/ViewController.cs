﻿using System;

using UIKit;

namespace GooglyFeud.iOS
{
	public partial class ViewController : UIViewController
	{
		public ViewController(IntPtr handle) : base(handle)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			txtSearch.ShouldReturn += delegate {
				GetQueries();
				return true;
			};

			btnSearch.AccessibilityIdentifier = "btnSearch";
			btnSearch.TouchUpInside += delegate
			{
				GetQueries();
			};

			//tried unchecking "user enteraction enabled" on the storyboard but it won't build then... idk what's up with that
			txtboxSuggestions.UserInteractionEnabled = false;
		}

		public void GetQueries()
		{
			var inputQuery = txtSearch.Text;

			var rawSuggestionList = GooglyFeud.Utils.FetchQueriesFromGoogle(inputQuery);

			var cleanedSuggestionsString = string.Empty;
			if (rawSuggestionList.Count > 0)
			{
				//todo: make individual answer boxes
				//stubbed into one big string for now
				foreach (var query in rawSuggestionList)
				{
					cleanedSuggestionsString += query + "\n";
				}

				txtboxSuggestions.Text = cleanedSuggestionsString;
			}
			else
			{
				txtboxSuggestions.Text = "No suggestions.";
			}
			txtSearch.ResignFirstResponder();
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.		
		}
	}
}
