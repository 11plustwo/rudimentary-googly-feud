﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;

namespace GooglyFeud.Droid
{
	public class GameFragment : Android.Support.V4.App.Fragment
	{
		View view;
		TextView edittextQuery;
		TextView txtSuggestions;
		Button btnSearch;

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView(inflater, container, savedInstanceState);

			view = inflater.Inflate(Resource.Layout.GameLayout, container, false);

			edittextQuery = view.FindViewById<TextView>(Resource.Id.edittextQuery);
			txtSuggestions = view.FindViewById<TextView>(Resource.Id.txtSuggestions);

			btnSearch = view.FindViewById<Button>(Resource.Id.btnSearch);
			btnSearch.Click += delegate
			{
				GetQueries();
			};
 




			return view;
		}

		public void GetQueries()
		{
			var rawSuggestionList = GooglyFeud.Utils.FetchQueriesFromGoogle(edittextQuery.Text);

			var cleanedSuggestionsString = string.Empty;
			if (rawSuggestionList.Any())
			{
				//todo: make individual answer boxes
				//stubbed into one big string for now
				foreach (var query in rawSuggestionList)
				{
					cleanedSuggestionsString += query + "\n";
				}

				txtSuggestions.Text = cleanedSuggestionsString;
			}
			else
			{
				txtSuggestions.Text = "No suggestions.";
			}

			InputMethodManager imm = (InputMethodManager) Context.GetSystemService(Context.InputMethodService);
			imm.HideSoftInputFromWindow(view.WindowToken, 0);
		}

	}
}
