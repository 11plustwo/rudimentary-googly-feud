﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace GooglyFeud.Droid
{
	public class MenuFragment : Android.Support.V4.App.Fragment
	{
		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView(inflater, container, savedInstanceState);

			var view = inflater.Inflate(Resource.Layout.MenuLayout, container, false);

			TextView txtTutorial = view.FindViewById<TextView>(Resource.Id.txtTutorial);
			txtTutorial.Click += delegate
			{
				var fragment = new TutorialFragment();
				var trans = Activity.SupportFragmentManager.BeginTransaction();
				trans.Replace(Resource.Id.fragmentContainer, fragment);
				trans.AddToBackStack(null);
				trans.Commit();
			};

			TextView txtContribute = view.FindViewById<TextView>(Resource.Id.txtContribute);
			txtContribute.Click += delegate
			{
				var fragment = new ContributeFragment();
				var trans = Activity.SupportFragmentManager.BeginTransaction();
				trans.Replace(Resource.Id.fragmentContainer, fragment);
				trans.AddToBackStack(null);
				trans.Commit();
			};

			TextView txtClearHistory = view.FindViewById<TextView>(Resource.Id.txtClearHistory);
			txtClearHistory.Click += delegate
			{
				//todo: clear history
				var toast = Toast.MakeText(Activity, "stub for clear history", ToastLength.Short);
				toast.Show();
			};

			TextView txtGetLatestSearches = view.FindViewById<TextView>(Resource.Id.txtGetLatestSearches);
			txtGetLatestSearches.Click += delegate
			{
				//todo: get latest
				var toast = Toast.MakeText(Activity, "stub for get latest", ToastLength.Short);
				toast.Show();
			};

			TextView txtPlay = view.FindViewById<TextView>(Resource.Id.txtPlay);
			txtPlay.Click += delegate
			{
				var fragment = new GameFragment();
				var trans = Activity.SupportFragmentManager.BeginTransaction();
				trans.Replace(Resource.Id.fragmentContainer, fragment);
				trans.AddToBackStack(null);
				trans.Commit();
			};

			return view;

		}
	}
}
