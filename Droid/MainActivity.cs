﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;

namespace GooglyFeud.Droid
{
	//todo: remove ConfigChanges which currently prevents view destruction on rotation when I implement seperate views for landscape/portrait
	[Activity(Label = "Googly Feud", MainLauncher = true, Icon = "@mipmap/icon", 
	          WindowSoftInputMode = Android.Views.SoftInput.AdjustResize,
	          ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | 
	          Android.Content.PM.ConfigChanges.KeyboardHidden | 
	          Android.Content.PM.ConfigChanges.Keyboard | 
	          Android.Content.PM.ConfigChanges.ScreenSize)]
	public class MainActivity : AppCompatActivity
	{
		Android.Support.V7.Widget.Toolbar mToolbar;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.MainLayout);

			SetSupportActionBar(mToolbar);

			var fragment = SupportFragmentManager.FindFragmentById(Resource.Id.fragmentContainer);
			if (fragment == null)
			{
				var trans = SupportFragmentManager.BeginTransaction();
				trans.Add(Resource.Id.fragmentContainer, new MenuFragment());
				trans.Commit();
			}

		}
	}
}


